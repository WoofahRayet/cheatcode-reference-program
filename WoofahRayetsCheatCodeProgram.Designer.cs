﻿namespace Cheatcode_Reference_Program
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.TheSims3CheatsButton = new System.Windows.Forms.Button();
            this.MadeByButton = new System.Windows.Forms.Button();
            this.CloseMainWindow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TheSims3CheatsButton
            // 
            this.TheSims3CheatsButton.Location = new System.Drawing.Point(13, 13);
            this.TheSims3CheatsButton.Name = "TheSims3CheatsButton";
            this.TheSims3CheatsButton.Size = new System.Drawing.Size(104, 23);
            this.TheSims3CheatsButton.TabIndex = 2;
            this.TheSims3CheatsButton.Text = "The Sims 3 Cheats";
            this.TheSims3CheatsButton.UseVisualStyleBackColor = true;
            this.TheSims3CheatsButton.Click += new System.EventHandler(this.TheSims3Button_Click);
            // 
            // MadeByButton
            // 
            this.MadeByButton.Location = new System.Drawing.Point(12, 426);
            this.MadeByButton.Name = "MadeByButton";
            this.MadeByButton.Size = new System.Drawing.Size(75, 23);
            this.MadeByButton.TabIndex = 3;
            this.MadeByButton.Text = "Made by";
            this.MadeByButton.UseVisualStyleBackColor = true;
            this.MadeByButton.Click += new System.EventHandler(this.MadeByButton_Click);
            // 
            // CloseMainWindow
            // 
            this.CloseMainWindow.Location = new System.Drawing.Point(297, 426);
            this.CloseMainWindow.Name = "CloseMainWindow";
            this.CloseMainWindow.Size = new System.Drawing.Size(75, 23);
            this.CloseMainWindow.TabIndex = 4;
            this.CloseMainWindow.Text = "Close";
            this.CloseMainWindow.UseVisualStyleBackColor = true;
            this.CloseMainWindow.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 461);
            this.Controls.Add(this.CloseMainWindow);
            this.Controls.Add(this.MadeByButton);
            this.Controls.Add(this.TheSims3CheatsButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "WoofahRayet\'s Cheatcode Reference Program";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button TheSims3CheatsButton;
        private System.Windows.Forms.Button MadeByButton;
        private System.Windows.Forms.Button CloseMainWindow;
    }
}

