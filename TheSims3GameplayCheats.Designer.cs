﻿namespace Cheatcode_Reference_Program
{
    partial class TheSims3GameplayCheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheSims3GameplayCheats));
            this.TheSims3GameplayCheatsCloseButton = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TheSims3GameplayCheatsCloseButton
            // 
            this.TheSims3GameplayCheatsCloseButton.Location = new System.Drawing.Point(713, 206);
            this.TheSims3GameplayCheatsCloseButton.Name = "TheSims3GameplayCheatsCloseButton";
            this.TheSims3GameplayCheatsCloseButton.Size = new System.Drawing.Size(75, 23);
            this.TheSims3GameplayCheatsCloseButton.TabIndex = 0;
            this.TheSims3GameplayCheatsCloseButton.Text = "Close";
            this.TheSims3GameplayCheatsCloseButton.UseVisualStyleBackColor = true;
            this.TheSims3GameplayCheatsCloseButton.Click += new System.EventHandler(this.TheSims3GameplayCheatsCloseButton_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(776, 188);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // TheSims3GameplayCheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 235);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.TheSims3GameplayCheatsCloseButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TheSims3GameplayCheats";
            this.Text = "The Sims 3 Gameplay Cheats";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button TheSims3GameplayCheatsCloseButton;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}