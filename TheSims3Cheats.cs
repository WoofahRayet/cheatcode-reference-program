﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cheatcode_Reference_Program
{
    public partial class TheSims3Cheats : Form
    {
        public TheSims3Cheats()
        {
            InitializeComponent();
        }

        private void TheSims3Cheats_Load(object sender, EventArgs e)
        {
        
        }

        private void TheSims3CheatsCloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MiscCheats_Click(object sender, EventArgs e)
        {
            Hide();
            TheSims3MiscCheats TheSims3MiscCheats = new TheSims3MiscCheats();
            TheSims3MiscCheats.ShowDialog();
            Show();
        }

        private void StorytellingCheats_Click(object sender, EventArgs e)
        {
            Hide();
            TheSims3StorytellingCheats TheSims3StorytellingCheats = new TheSims3StorytellingCheats();
            TheSims3StorytellingCheats.ShowDialog();
            Show();
        }

        private void BuildCheatsButton_Click(object sender, EventArgs e)
        {
            Hide();
            TheSims3BuildCheats TheSims3BuidCheats = new TheSims3BuildCheats();
            TheSims3BuidCheats.ShowDialog();
            Show();
        }

        private void GameplayCheatsButton_Click(object sender, EventArgs e)
        {
            Hide();
            TheSims3GameplayCheats TheSims3GameplayCheats = new TheSims3GameplayCheats();
            TheSims3GameplayCheats.ShowDialog();
            Show();
        }

        private void AdvancedCheatsButton_Click(object sender, EventArgs e)
        {
            Hide();
            TheSims3AdvancedCheats TheSims3AdvancedCheats = new TheSims3AdvancedCheats();
            TheSims3AdvancedCheats.ShowDialog();
            Show();
        }

        private void TestingCheatsButton_Click(object sender, EventArgs e)
        {
            Hide();
            TheSims3TestingCheats TheSims3TestingCheats = new TheSims3TestingCheats();
            TheSims3TestingCheats.ShowDialog();
            Show();

        }
    }
}

