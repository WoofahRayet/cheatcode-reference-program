﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cheatcode_Reference_Program
{
    public partial class TheSims3TestingCheats : Form
    {
        public TheSims3TestingCheats()
        {
            InitializeComponent();
        }

        private void TheSims3TestingCheatsCloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
