﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cheatcode_Reference_Program
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
           
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MadeByButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Developed by WoofahRayet");
        }

        private void TheSims3Button_Click(object sender, EventArgs e)
        {
            Hide();
            TheSims3Cheats TheSims3Cheats = new TheSims3Cheats();
            TheSims3Cheats.ShowDialog();
            Show();
        }

    }
}
